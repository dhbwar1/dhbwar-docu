# Use-Case Specification: Place Troops


## 1. Use-Case Name 
### 1.1 Brief Description
The use case allows the user to Place Troops 

## 2. Flow of Events
### 2.1 Basic Flow 
Activity Diagram Diagram: 

<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/ActivityDiagrams/ActivityPlace.png" alt="activity diagram" />

### 2.2 Mock-Up
NA
### 2.3 .feature file

feature file: 

## 3. Special Requirements
### 3.1 
## 4. Preconditions
User needs enough mana Points to place a troop

## 5. Postconditions
### 5.1 Game

## 6. Function Points
<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/pictures_fp/place_troops_1.PNG" alt="activity diagram" />
<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/pictures_fp/place_troops_2.PNG" alt="activity diagram" />
