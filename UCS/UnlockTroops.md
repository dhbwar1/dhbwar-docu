# Use-Case Specification: Unlock Troops


## 1. Use-Case Name 
### 1.1 Brief Description
The use case allows the user to unlock new Troops 

## 2. Flow of Events
### 2.1 Basic Flow 
Activity Diagram Diagram: 

<img src="" alt="activity diagram" />

### 2.2 Mock-Up
Mock-Up:

<img src=""/>

### 2.3 .feature file

feature file: 

## 3. Special Requirements

## 4. Preconditions


## 5. Postconditions

## 6. Function Points
<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/pictures_fp/unlock_troops_1.PNG" alt="activity diagram" />
<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/pictures_fp/unlock_troops_2.PNG" alt="activity diagram" />
