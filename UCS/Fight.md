# Use-Case Specification: Fighting Troops


## 1. Use-Case Name 
### 1.1 Brief Description
The use case allows the troops to fight each other 

## 2. Flow of Events
### 2.1 Basic Flow 
Activity Diagram Diagram: 

<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/ActivityDiagrams/ActivityFight.png" alt="activity diagram" />

### 2.2 Mock-Up
Mock-Up:



### 2.3 .feature file

feature file: 

## 3. Special Requirements


## 4. Preconditions
The Troops need to be in range to fight

## 5. Postconditions


## 6. Function Points
n/a
