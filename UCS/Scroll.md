# Use-Case Specification: Start Game


## 1. Use-Case Name 
### 1.1 Brief Description
The use case allows the user to Scroll through the playing field 

## 2. Flow of Events
### 2.1 Basic Flow 
Activity Diagram Diagram: 

<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/ActivityDiagrams/ActivityScroll.png" alt="activity diagram" />

### 2.2 Mock-Up
Mock-Up:



### 2.3 .feature file

feature file: 

## 3. Special Requirements

## 4. Preconditions


## 5. Postconditions
### 5.1 Game
When Scrolling the positons of troops need to be calculated again

## 6. Function Points
n/a
