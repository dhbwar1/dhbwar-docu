# Software Architecture Document


## 1. Introduction 
### 1.1 Purpose
This document provides a comprehensive architectural overview of the system, 
using a number of different architectural views to depict different aspects of the system. 
It is intended to capture and convey the significant architectural decisions which have been made on the system.

### 1.2 Scope
The scope of this SAD is to show the architecture of the vnv project. Affected are the class structure, the use cases and the data representation.

### 1.3 References
- [GitHub](https://gitlab.com/dhbwar1/dhbwar)
- [Blog](https://dhbwar.wordpress.com/)
- [Documentation](https://gitlab.com/dhbwar1/dhbwar-docu)
- [Software Requirements Specification](ReadMe.md)


## 2. Architectural Representation
<img src ="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/MVC/MVC.png" alt ="AR"/>

## 3.Architectural Goals and Constraints 
Since we use Andriod we dont use a generated MVC. It's important for us to seperate the view and the model, so we could replace the view easily (maybe for a PC-port).

## 4. Use-Case View 
<img src ="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/Scope/ScopeMidterm.png" alt = "UC"/>

## 5. Logical View
<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/MVC/UMLMVC.png" alt= "MVC">


## 6. Process View


## 7. Deployment View
Using APK

<img src ="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/apk.png" alt= "apk">

## 8. Implementation View
<img src ="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/UCS/OldUMLnoPattern.png" alt= "UML">

### New Design Pattern
<img src ="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/UCS/NewClassUMLwithPatern.png" alt = "UML">

## 9. Data View


## 10. Size and Performance
tbd

## 11. Quality



