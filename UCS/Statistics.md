# Use-Case Specification: Statistics


## 1. Use-Case Name 
### 1.1 Brief Description
The use case allows the user to view the Statistics

## 2. Flow of Events
### 2.1 Basic Flow 
Activity Diagram Diagram: 

<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/ActivityDiagrams/ActivityMenuStatistics.png" alt="activity diagram" />

### 2.2 Mock-Up
Mock-Up:

<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/MockUps/StatisticsMockup.png"/>

### 2.3 .feature file

feature file: 

## 3. Special Requirements
### 3.1 Username
The user needs to enter a Username

## 4. Preconditions


## 5. Postconditions


## 6. Function Points
<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/pictures_fp/view_stats_and_change_settings_1.PNG"/>
<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/pictures_fp/view_stats_and_change_settings_2.PNG"/>
