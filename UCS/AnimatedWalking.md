# Use-Case Specification: Start Game


## 1. Use-Case Name 
### 1.1 Brief Description
The use case shows the Troops walking 

## 2. Flow of Events
### 2.1 Basic Flow 
Activity Diagram Diagram: 

<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/ActivityDiagrams/ActivityAnimate.png" alt="activity diagram" />

### 2.2 Mock-Up
Mock-Up:



### 2.3 .feature file

feature file: 

## 3. Special Requirements

## 4. Preconditions
A Troops has to be Placed

## 5. Postconditions

## 6. Function Points
<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/pictures_fp/animated_walking_1.PNG" alt="activity diagram" />
<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/pictures_fp/animated_walking_2.PNG" alt="activity diagram" />
