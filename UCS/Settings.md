# Use-Case Specification: Settigns


## 1. Use-Case Name 
Change Settigns
### 1.1 Brief Description
The use case allows the user to change Settings 

## 2. Flow of Events
### 2.1 Basic Flow 
Activity Diagram Diagram: 

<img src="" alt="activity diagram" />

### 2.2 Mock-Up
Mock-Up:

<img src=""/>

### 2.3 .feature file

feature file: 

## 3. Special Requirements


## 4. Preconditions


## 5. Postconditions


## 6. Function Points
<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/pictures_fp/view_stats_and_change_settings_1.PNG" alt="activity diagram" />
<img src="https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/pictures_fp/view_stats_and_change_settings_2.PNG" alt="activity diagram" />
