# Dhbwar - Software Requirements Specification

## Table of Contents

-   [Flashcard Community - Software Requirements Specification](#flashcard-community---software-requirements-specification)

    -   [Table of Contents](#table-of-contents)

    -   [1. Introduction](#1-introduction)

        -   [1.1 Purpose](#11-purpose)
        -   [1.2 Scope](#12-scope)
        -   [1.3 Definitions, Acronyms and Abbreviations](#13-definitions-acronyms-and-abbreviations)
        -   [1.4 References](#14-references)
        -   [1.5 Overview](#15-overview)

    -   [2. Overall Description](#2-overall-description)

        -   [2.1 Vision](#21-vision)

        -   [2.2 Use Case Diagram](#22-use-case-diagram)

        -   [2.3 Technology Stack](#23-technology-stack)
        

    -   [3. Specific Requirements](#3-specific-requirements)

        -   [3.1 Functionality – Data Backend](#31-functionality--data-backend)

            -   [3.1.1 Read data given over API endpoints](#311-read-data-given-over-api-endpoints)
            -   [3.1.2 Parse data](#312-parse-data)
            -   [3.1.3 Provide data](#313-provide-data)

        -   [3.2 Functionality – User Interface](#32-functionality--user-interface)

            -   [3.2.1 User system](#321-user-system)
            -   [3.2.3 Flashcard boxes](#323-flashcard-boxes)
            -   [3.2.4 Flashcards](#324-flashcards)
            -   [3.2.5 Statistics](#325-statistics)

        -   [3.3 Usability](#33-usability)

        -   [3.4 Reliability](#34-reliability)

            -   [3.4.1 Availability](#341-availability)
            -   [3.4.2 MTBF, MTTR](#342-mtbf-mttr)
            -   [3.4.3 Accuracy](#343-accuracy)
            -   [3.4.4 Bug classes](#344-bug-classes)

        -   [3.5 Performance](#35-performance)

            -   [3.5.1 Response time](#351-response-time)
            -   [3.5.2 Throughput](#352-throughput)
            -   [3.5.3 Capacity](#353-capacity)
            -   [3.5.4 Resource utilization](#354-resource-utilization)

        -   [3.6 Supportability](#36-supportability)

        -   [3.7 Design Constraints](#37-design-constraints)

            -   [3.7.1 Development tools](#371-development-tools)
            -   [3.7.2 Spring Boot](#372-spring-boot)
            -   [3.7.3 ReactJS](#373-reactjs)
            -   [3.7.4 Supported Platforms](#374-supported-platforms)

        -   [3.8 Online User Documentation and Help System Requirements](#38-online-user-documentation-and-help-system-requirements)

        -   [3.9 Purchased Components](#39-purchased-components)

        -   [3.10 Interfaces](#310-interfaces)

            -   [3.10.1 User Interfaces](#3101-user-interfaces)
            -   [3.10.2 Hardware Interfaces](#3102-hardware-interfaces)
            -   [3.10.3 Software Interfaces](#3103-software-interfaces)
            -   [3.10.4 Communications Interfaces](#3104-communications-interfaces)

        -   [3.11 Licensing Requirements](#311-licensing-requirements)

        -   [3.12 Legal, Copyright and other Notices](#312-legal-copyright-and-other-notices)

        -   [3.13 Applicable Standards](#313-applicable-standards)

    -   [4. Supporting Information](#4-supporting-information)

## 1. Introduction

### 1.1 Purpose

This Software Requirements Specification (SRS) describes all specifications for the application "Dhbwar". It includes an overview about this project and its vision, detailed information about the planned features and boundary conditions of the development process.

### 1.2 Scope

The Project is going to be an Android App.

Actors of this App are only users.

By Midterm we want to implement a menue you can navigate through and start a singleplayergame. Ingame you should be able to place troops, scroll through the map and let the troops fight.

Planned Subsystems are:

    -Connecting to local games:
    One deivce will be hosting the game and the other device will be the client.
    
    -Online Multiplayer:
    You can search for games in a list of other people that are looking for games at the moment.
    
    -Account System:
    User create accounts so they can save their statistics and find games of their friends more easily when a lot of people              are searching for games.
    
    -Singleplayer:
    User can Play against a KI
    

### 1.3 Definitions, Acronyms and Abbreviations


| Term     |                                     |
| -------- | ----------------------------------- |
| **FAQ**  | Frequently Asked Questions          |


### 1.4 References


| Title                                                                                                 | Date       |
| ----------------------------------------------------------------------------------------------------- | ---------- |
| [Blog](https://dhbwar.wordpress.com/)                                                                 | 11/06/2021 |
| [GitLab](https://gitlab.com/dhbwar1/dhbwar)                                                           | 11/06/2021 |
| [Current Issues](https://bit.ly/375Mpl4)                                                              | 11/06/2021 |

### 1.5 Overview

The following chapter provides an overview of this project with vision and Overall Use Case Diagram. The third chapter (Requirements Specification) delivers more details about the specific requirements in terms of functionality, usability and design parameters. Finally there is a chapter with supporting information.

## 2. Overall Description

### 2.1 Vision

Dhbwar is a 2d side-scroller tower-defense game inspired by legendary games like cartoon wars and age of war. We want to bring back those games with a new and fresh dhbw theme. You go through the different semesters while fighting exmatriculation and upgrading your skills. There will be a singleplayer-mode where you can train your skills and perfect you tactics and when you are ready you can test those skills against other players in real-time in the mulitplayer-mode via a local-network or the world wide web. In the game you can place Troops by buying them with ECTS that you earn over time. While playing you move up in semesters and get more powerfull troops while doing so. You win by destroying the enemies base and getting your bachelors degree 


### 2.2 Use Case Diagram

[Use Case Diagram](https://gitlab.com/dhbwar1/dhbwar-docu/-/raw/master/Untitled%20Diagram.png)

[Featurefiles](https://gitlab.com/dhbwar1/dhbwar/-/blob/master/test.feature)

### 2.3 Technology Stack

The Technology we use is:

Frontend: -Android with Java and XML
Backend : -Coming soon 

IDE: -IntelliJ and Android Studio

Project Management: -YouTrack 
                    -GitLab

## 3. Specific Requirements

### 3.1 Functionality – Data Backend

#### 3.1.1 Read data given over API endpoints

#### 3.1.2 Parse data

#### 3.1.3 Provide data

### 3.2 Functionality – User Interface

#### 3.2.1 User system

#### 3.2.3 Flashcard boxes

#### 3.2.4 Flashcards

#### 3.2.5 Statistics

### 3.3 Usability

### 3.4 Reliability

#### 3.4.1 Availability

#### 3.4.2 MTBF, MTTR

#### 3.4.3 Accuracy

#### 3.4.4 Bug classes

### 3.5 Performance

#### 3.5.1 Response time

#### 3.5.2 Throughput

#### 3.5.3 Capacity

#### 3.5.4 Resource utilization

### 3.6 Supportability

### 3.7 Design Constraints

#### 3.7.1 Development tools

#### 3.7.2 Spring Boot

#### 3.7.3 ReactJS

#### 3.7.4 Supported Platforms

### 3.8 Online User Documentation and Help System Requirements

### 3.9 Purchased Components

### 3.10 Interfaces

#### 3.10.1 User Interfaces

#### 3.10.2 Hardware Interfaces

#### 3.10.3 Software Interfaces

#### 3.10.4 Communications Interfaces

### 3.11 Licensing Requirements

### 3.12 Legal, Copyright and other Notices

### 3.13 Applicable Standards

## 4. Supporting Information
